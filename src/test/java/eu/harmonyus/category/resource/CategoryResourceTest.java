package eu.harmonyus.category.resource;

import eu.harmonyus.category.model.CategoryEntity;
import eu.harmonyus.category.resource.model.Category;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.http.HttpHeaders;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class CategoryResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @BeforeEach
    void setup() {
        CategoryEntity.deleteAll().await().indefinitely();
    }

    @Test
    @DisplayName("Test - When Calling POST - /api/{version}/categories should create resource - 201")
    void testCreateCategory() {
        Category category = createCategory("test category", null);

        given()
                .when()
                .body(category)
                .contentType(ContentType.JSON)
                .post(restPath + "/categories")
                .then()
                .statusCode(201)
                .header(HttpHeaders.LOCATION.toString(), containsString(restPath + "/categories/test-category"))
                .body("name", is("test category"))
                .body("code", is("test-category"));
    }

    @Test
    @DisplayName("Test - When Calling DELETE - /api/{version}/categories/{id} should delete resource - 204")
    void testDeleteCategory() {
        createCategoryEntity("test category");

        given()
                .when()
                .get(restPath + "/categories/test-category")
                .then()
                .statusCode(200);

        given()
                .when()
                .delete(restPath + "/categories/test-category")
                .then()
                .statusCode(204);

        given()
                .when()
                .get(restPath + "/categories/test-category")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Test - When Calling PUT - /api/{version}/categories/{id} should update resource - 200")
    void testUpdateCategory() {
        createCategoryEntity("ategory");

        given()
                .when()
                .get(restPath + "/categories/ategory")
                .then()
                .statusCode(200);

        Category category = createCategory("test category", "test-category");

        given()
                .when()
                .body(category)
                .contentType(ContentType.JSON)
                .put(restPath + "/categories/ategory")
                .then()
                .statusCode(200);

        given()
                .when()
                .get(restPath + "/categories/test-category")
                .then()
                .statusCode(200)
                .body("name", is("test category"))
                .body("code", is("test-category"));
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/categories should return resources - 200")
    void testGetAll() {
        createCategoryEntity("Category one");
        createCategoryEntity("Category two");
        createCategoryEntity("Category three");

        Category[] categories =  given()
                .when()
                .get(restPath + "/categories")
                .then()
                .statusCode(200)
                .extract()
                .as(Category[].class);
        assertThat("Categories length", categories.length, equalTo(3));
        assertCategoryValue(categories[0], "Category one", "category-one");
        assertCategoryValue(categories[1], "Category two","category-two");
        assertCategoryValue(categories[2], "Category three","category-three");
    }

    private void assertCategoryValue(Category category, String name, String code) {
        assertThat(category.name, equalTo(name));
        assertThat(category.code, equalTo(code));
    }

    private void createCategoryEntity(String name) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName(name);
        categoryEntity.persist().await().indefinitely();
    }

    private Category createCategory(String name, String code) {
        Category category = new Category();
        category.name = name;
        category.code = code;
        return category;
    }
}
