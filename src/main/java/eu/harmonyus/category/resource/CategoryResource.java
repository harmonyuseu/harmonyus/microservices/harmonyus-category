package eu.harmonyus.category.resource;

import eu.harmonyus.category.model.CategoryEntity;
import eu.harmonyus.category.resource.mapper.CategoryMapper;
import eu.harmonyus.category.resource.model.Category;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Duration;
import java.util.List;

@Path("/categories")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {

    @ConfigProperty(name = "harmonyus.category-resource.get-all.timeout", defaultValue = "2s")
    Duration getAllTimeout;
    @ConfigProperty(name = "harmonyus.category-resource.get-by-code.timeout", defaultValue = "2s")
    Duration getByCodeTimeout;
    @ConfigProperty(name = "harmonyus.category-resource.create.timeout", defaultValue = "2s")
    Duration createTimeout;
    @ConfigProperty(name = "harmonyus.category-resource.modify.timeout", defaultValue = "2s")
    Duration modifyTimeout;
    @ConfigProperty(name = "harmonyus.category-resource.delete.timeout", defaultValue = "2s")
    Duration deleteTimeout;
    @ConfigProperty(name = "harmonyus.category-resource.get-all.max-retries", defaultValue = "1")
    long getAllMaxRetries;
    @ConfigProperty(name = "harmonyus.category-resource.get-by-code.max-retries", defaultValue = "1")
    long getByCodeMaxRetries;
    @ConfigProperty(name = "harmonyus.category-resource.create.max-retries", defaultValue = "1")
    long createMaxRetries;
    @ConfigProperty(name = "harmonyus.category-resource.modify.max-retries", defaultValue = "1")
    long modifyMaxRetries;
    @ConfigProperty(name = "harmonyus.category-resource.delete.max-retries", defaultValue = "1")
    long deleteMaxRetries;

    @GET
    public Uni<List<Category>> getAll() {
        Uni<List<CategoryEntity>> categoriesEntitiesUni = CategoryEntity.listAll();
        return categoriesEntitiesUni
                .ifNoItem().after(getAllTimeout).fail()
                .onItem().transform(CategoryMapper::fromEntityList)
                .onFailure().retry().atMost(getAllMaxRetries);
    }

    @GET
    @Path("/{code}")
    public Uni<Category> getByCode(@PathParam("code") String code) {
        Uni<CategoryEntity> categoryEntityUni = CategoryEntity.findById(code);
        return categoryEntityUni
                .ifNoItem().after(getByCodeTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .map(CategoryMapper::fromEntity)
                .onFailure().retry().atMost(getByCodeMaxRetries);
    }

    @POST
    public Uni<Response> create(Category category) {
        CategoryEntity categoryEntity = new CategoryEntity();
        CategoryMapper.toEntity(categoryEntity, category);
        return categoryEntity.persist()
                .ifNoItem().after(createTimeout).fail()
                .map(unused -> Response.created(URI.create(String.format("/categories/%s", categoryEntity.getCode())))
                        .entity(CategoryMapper.fromEntity(categoryEntity)).build())
                .onFailure().retry().atMost(createMaxRetries);
    }

    @PUT
    @Path("/{code}")
    public Uni<Category> modify(@PathParam("code") String code, Category category) {
        Uni<CategoryEntity> categoryEntityUni = CategoryEntity.findById(code);
        return categoryEntityUni
                .ifNoItem().after(modifyTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(categoryEntity -> {
                    CategoryMapper.toEntity(categoryEntity, category);
                    return categoryEntity.persistOrUpdate().onItem().transform(unused -> categoryEntity);
                })
                .onItem().transform(CategoryMapper::fromEntity)
                .onFailure().retry().atMost(modifyMaxRetries);
    }

    @DELETE
    @Path("/{code}")
    public Uni<Response> delete(@PathParam("code") String code) {
        Uni<CategoryEntity> categoryEntityUni = CategoryEntity.findById(code);
        return categoryEntityUni
                .ifNoItem().after(deleteTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(ReactivePanacheMongoEntityBase::delete)
                .map(unused -> Response.noContent().build())
                .onFailure().retry().atMost(deleteMaxRetries);
    }
}
