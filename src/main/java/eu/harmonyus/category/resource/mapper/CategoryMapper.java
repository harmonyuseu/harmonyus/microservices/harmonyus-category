package eu.harmonyus.category.resource.mapper;

import eu.harmonyus.category.model.CategoryEntity;
import eu.harmonyus.category.resource.model.Category;

import java.util.ArrayList;
import java.util.List;

public final class CategoryMapper {

    private CategoryMapper() {

    }

    public static Category fromEntity(CategoryEntity categoryEntity) {
        Category category = new Category();
        category.code = categoryEntity.getCode();
        category.name = categoryEntity.getName();
        return category;
    }

    public static List<Category> fromEntityList(List<CategoryEntity> categoriesEntities) {
        List<Category> categories = new ArrayList<>();
        categoriesEntities.forEach(categoryEntity -> categories.add(fromEntity(categoryEntity)));
        return categories;
    }

    public static void toEntity(CategoryEntity categoryEntity, Category category) {
        if (category.code != null) {
            categoryEntity.setCode(category.code);
        }
        categoryEntity.setName(category.name);
    }
}
