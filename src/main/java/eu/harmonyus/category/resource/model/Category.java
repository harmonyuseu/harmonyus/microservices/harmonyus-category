package eu.harmonyus.category.resource.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Category {
    public String name;
    public String code;
}
