package eu.harmonyus.category.model;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.bson.codecs.pojo.annotations.BsonId;

import java.text.Normalizer;
import java.time.LocalDateTime;

@MongoEntity(collection = "category")
public class CategoryEntity extends ReactivePanacheMongoEntityBase {

    @BsonId
    private String code;

    private String name;
    private LocalDateTime creation = null;
    private LocalDateTime modification = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getModification() {
        return modification;
    }

    public void setModification(LocalDateTime modification) {
        this.modification = modification;
    }

    @Override
    public Uni<Void> persist() {
        if (this.code == null) {
            this.code = createCode(name);
        }
        if (creation == null) {
            creation = LocalDateTime.now();
        }
        return super.persist();
    }

    @Override
    public Uni<Void> persistOrUpdate() {
        if (modification == null) {
            modification = LocalDateTime.now();
        }
        return super.persistOrUpdate();
    }

    private static String createCode(String value) {
        return stripAccents(value.toLowerCase()).replace(" ", "-").replace("'", "-");
    }

    private static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
}